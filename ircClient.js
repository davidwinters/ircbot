'use strict';
{
	let irc = require('irc');
	
	let handlers = [];
	
	class IRCClient {
		constructor(server, nick, channels, messageHandlers) {
			this.client = null;
			this.server = server;
			this.nick = nick;
			this.channels = channels;
			handlers = messageHandlers;
			
			this.connectToIRC(this.server, this.nick, this.channels);
		}
		
		connectToIRC(server, nick, channels) {
			console.log(`Connecting to ${server} ${channels} as ${nick}.`);
			
			this.client = new irc.Client(server, nick, {
				channels: channels
			});
			
			this.client.addListener('error', (message) => { this.errorHandler });
			this.client.addListener('message', this.messageHandler);
		}
		
		errorHandler(message) {
			console.log('ERROR: ' + message);
		}
		
		messageHandler(from, to, message) {
			for(let i = 0; i < handlers.length; i++) {
				try {
					handlers[i].handleMessage(this, from, to, message);	
				}
				catch(err) {
					this.say(to, 'NOPE! ' + err);	
				}						
			}
		}
		
		say(channel, message) {
			this.client.say(channel, message);
			console.log('SELF => ' + channel + ': ' + message);
		}		
	}

	exports.IRCClient = IRCClient;
}
