'use strict';
{
	class Logger {
		constructor(db) {
			this.db = db;
			this.lastFace = null;
		}
		
		handleMessage(client, from, to, message) {
			this.logMessage(from, to, message);
		}
		
		logMessage(from, to, message) {
			let collection = this.db.get('logs');
			collection.insert({ 
				'type': 'message',
				'time': new Date(),
				'from': from,
				'lowercaseFrom': from.toLowerCase(), // this is stupid. there's got to be a better way to handle this
				'to': to,
				'message': message
			}, function (err, doc) {
				if (err) {
					console.log('Error adding log: ' + err);
				}
				else {
					console.log(from + ' => ' + to + ': ' + message);
				}
			});	
		}
	}
	
	exports.Logger = Logger;
}




