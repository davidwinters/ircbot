'use strict';
{
	let mongo = require('mongodb');
	let monk = require('monk');
	
	let IRCClient = require('./ircClient').IRCClient;
	let Faces = require('./MessageHandlers/faces').Faces;
	let Lols = require('./MessageHandlers/lols').Lols;
	let Admin = require('./MessageHandlers/admin').Admin;
	let Logger = require('./MessageHandlers/logger').Logger;
	let Seen = require('./MessageHandlers/seen').Seen;
	let Partyhard = require('./MessageHandlers/partyhard').Partyhard;
	
	class Bot {
		constructor(server, nick, channels, dbConnectionString) {
			let db = monk(dbConnectionString);
			
			let messageHandlers = [];
			messageHandlers.push(new Faces(db));
			messageHandlers.push(new Lols(db));
			messageHandlers.push(new Admin());
			messageHandlers.push(new Logger(db));
			messageHandlers.push(new Seen(db));
			messageHandlers.push(new Partyhard());
			
			this.client = new IRCClient(server, nick, channels, messageHandlers);
		}
	}
	
	let bot = new Bot('irc.freenode.net', 'mybot', ['#mktest'], 'localhost:27017/ircbot');
}
